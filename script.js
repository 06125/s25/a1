/*
Find users with letter 'a' in ther first or last name
	use the $or operator
	show only the firstName and lastName and hide the _id field
*/
db.users.find(
    { $or:
    	[
    		{"firstName":
    			{$regex:"A", $options: "i"}
    		},
    		{"lastName":
    			{$regex:"A", $options:"i"}
    		}
    	]
    },
    {"firstName": 1, "lastName": 1, "_id":0}
)


/*
Find users who are admins and isActive
	use the $and operator
*/
db.users.find(
    { $and:
    	[
    		{"isAdmin":true},
    		{"isActive":true}
    	]
    }
)


/*
Find the courses with letter 'u' in its name and has a price of greater than or equal to 13000
	use the $regex and $gte operators
*/
db.courses.find(
    {$and:
    	[
    		{"name":{$regex:"u", $options:"i"}}, 
    		{"price":{$gte: 13000}}
    	]
    }
)